package com.itau.escolauhul.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.escolauhul.models.Aluno;
import com.itau.escolauhul.repositories.AlunoRepository;


@RestController
public class AlunoController {

	@Autowired
	AlunoRepository alunoRepository;

	// Um professor especifico
	@RequestMapping(method=RequestMethod.GET, path="/aluno/{id}")
	public ResponseEntity<Aluno> getAluno(@PathVariable long id) {
		Optional<Aluno> optionalAluno= alunoRepository.findById(id);
		if (!optionalAluno.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		return ResponseEntity.ok().body(optionalAluno.get());
	}
		


	//	 Todos os Alunos
	@RequestMapping(method=RequestMethod.GET, path="/alunos")
	public ResponseEntity<Iterable<Aluno>> getAlunos() {

		return ResponseEntity.ok().body(alunoRepository.findAll());
	}
	
//	Inserir Professor
	@RequestMapping(method=RequestMethod.POST, path="/aluno")
	public ResponseEntity<Aluno> inserirAluno(@Valid @RequestBody Aluno aluno) {
	
		try {
			alunoRepository.save(aluno);
			
			return ResponseEntity.ok().body(aluno);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
		
	
}
