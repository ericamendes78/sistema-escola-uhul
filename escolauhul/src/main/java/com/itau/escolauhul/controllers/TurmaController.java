package com.itau.escolauhul.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.escolauhul.models.Turma;
import com.itau.escolauhul.repositories.TurmaRepository;

@RestController
public class TurmaController {
	
	@Autowired
	TurmaRepository turmaRepository;
	
	@RequestMapping(path="turmas")
	public Iterable<Turma> buscarTurmas(){
		return turmaRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/turma/{id}")
	public ResponseEntity<?> buscarTurmaPorId(@PathVariable long id) {
		Optional<Turma> optionalTurma = turmaRepository.findById(id);
		
		if(!optionalTurma.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(optionalTurma.get());
	}
		
	@RequestMapping(method=RequestMethod.POST, path="/turma")
	public Turma inserirTurma(@Valid @RequestBody Turma turma) {
		return turmaRepository.save(turma);
	}

	@RequestMapping(method=RequestMethod.PUT, path="/turma/{id}")
	public ResponseEntity<?> alterarTurma(@PathVariable long id, @Valid @RequestBody Turma turma) {
		Optional<Turma> optionalTurma = turmaRepository.findById(id);
		
		if(!optionalTurma.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Turma turmaSalvo = turmaRepository.save(turma);
		
		return ResponseEntity.ok().body(turmaSalvo);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/turma/{id}")
	public ResponseEntity<?> deletarTurma(@PathVariable long id) {
		Optional<Turma> optionalTurma = turmaRepository.findById(id);
		
		if(!optionalTurma.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		turmaRepository.deleteById(id);
		
		return ResponseEntity.ok().build();
	}

}
