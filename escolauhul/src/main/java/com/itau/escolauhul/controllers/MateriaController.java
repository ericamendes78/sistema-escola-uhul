package com.itau.escolauhul.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.escolauhul.models.Materia;
import com.itau.escolauhul.repositories.MateriaRepository;

@RestController
public class MateriaController {

	@Autowired
	MateriaRepository materiaRepository;
	
	@RequestMapping(method=RequestMethod.GET, path="/materias")
	public Iterable<Materia> getMaterias() {
		return materiaRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/materia/{id}")
	public ResponseEntity<Materia> getMateria(@PathVariable String id) {
		Optional<Materia> materia = materiaRepository.findById(id);
		if (!materia.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(materia.get());
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/materia")
	public ResponseEntity<Materia> postMateria(@Valid @RequestBody Materia materia) {
		Optional<Materia> materiaExistente = materiaRepository.findById(materia.getNome());
		if (materiaExistente.isPresent()) {
			return ResponseEntity.status(400).build();
		}
		return ResponseEntity.ok(materiaRepository.save(materia));
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/materia/{id}")
	public ResponseEntity<Materia> putMateria(@Valid @PathVariable String id, @RequestBody Materia materia) {
		Optional<Materia> materiaExistente = materiaRepository.findById(materia.getNome());
		if (!materiaExistente.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		return ResponseEntity.ok(materiaRepository.save(materia));
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/materia/{id}")
	public ResponseEntity<Materia> putMateria(@Valid @PathVariable String id) {
		Optional<Materia> materiaExistente = materiaRepository.findById(id);
		if (!materiaExistente.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		materiaRepository.deleteById(id);
		return ResponseEntity.ok(materiaExistente.get());
	}
	
}
