package com.itau.escolauhul.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.escolauhul.models.Turma;

public interface TurmaRepository extends CrudRepository<Turma, Long> {
	
}
